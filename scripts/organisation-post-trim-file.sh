#!/bin/bash

# Execute this command after trimming

dossier_parent="$HOME"/uca_m2bi_hpc_lumartin_atac-seq/results/trimmed_data
find "$dossier_parent" -mindepth 2 -type f -exec mv -t "$dossier_parent" {} +

rmdir "$HOME"/uca_m2bi_hpc_lumartin_atac-seq/results/trimmed_data/[0-9]*