#!/bin/bash
#SBATCH --time=0:40:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
##SBATCH --mem=3G
#SBATCH --mem-per-cpu=2G
#SBATCH --cpus-per-task=2
#SBATCH --array=0-23
#SBATCH -o log/slurmjob-%A-%a
#SBATCH --job-name=qc_post
#SBATCH --partition=normal


echo 'Quality control post trimming'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose
#set -euo pipefail

IFS=$'\n\t'

# Upload whatever required packages
module load java/oracle-1.7.0_79 fastqc/0.11.7

echo "Set up directories ..." >&2
# Set up data and result directories
# Run QC on raw data

DATA_DIR="$HOME"/uca_m2bi_hpc_lumartin_atac-seq/results/trimmed_data

echo "Set up an array with all trim_fastq.gz sequence files..." >&2
tab=($(ls "$DATA_DIR"/*.fastq.gz))
echo "tab = " >&2
printf '%s\n' "${tab[@]}" >&2

# Set up the temporary directory
shortname=($(basename "${tab[$SLURM_ARRAY_TASK_ID]}" .fastq.gz ))
echo "shortname = " >&2
printf '%s\n' "${shortname[@]}" >&2

SCRATCHDIR=/storage/scratch/"$USER"/"$shortname"/qc_post

OUTPUT="$HOME"/uca_m2bi_hpc_lumartin_atac-seq/results/qc_post/"$shortname"

mkdir -p "$OUTPUT"
mkdir -p -m 700 "$SCRATCHDIR"

cd "$SCRATCHDIR"

# Run the program

echo "Start on $SLURMD_NODENAME: "`date` >&2

echo "Run the final quality control... " >&2

fastqc "${tab[$SLURM_ARRAY_TASK_ID]}" -dir "$SCRATCHDIR" -o "$SCRATCHDIR"
mv "$SCRATCHDIR" "$OUTPUT"

# Cleaning in case something went wrong
rm -rf  "$SCRATCHDIR"

echo "Stop job : "`date` >&2