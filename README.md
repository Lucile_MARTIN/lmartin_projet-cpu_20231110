# uca_m2bi_hpc_lumartin_atac-seq

## Description

Find here scripts and data set metadata from "STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse"
David Gomez-Cabrero et al. 2019
https://doi.org/10.1038/s41597-019-0202-7. 

Objective is to reproduce analysis on a HPC infrastructure using SLURM as scheduler on the ATAC-seq data.

Please, to run the scripts, clone my git repository.

```
git clone https://gitlab.com/Lucile_MARTIN/uca_m2bi_hpc_lumartin_atac-seq.git
```

To run stategra_wf.sh, please move in the git repository :
```bash
[student03@ws2023-master ~]$ cd uca_m2bi_hpc_lumartin_atac-seq
[student03@ws2023-master ~]$ bash stategra_wf.sh
```

## Requirements

This scripts can run on a HPC infrastructure using Lmod to manage environment. 

Please create two conda environment with this command : and activate conda to treate the data

```bash
[student03@ws2023-master ~]$ module load conda/4.12.0
[student03@ws2023-master ~]$ conda create -n hpcTools
[student03@ws2023-master ~]$ conda activate hpcTools
[student03@ws2023-master ~]$ conda install -c bioconda samtools=1.9 #latest version is not available on this version of conda
[student03@ws2023-master ~]$ conda install -c bioconda bedtools

# This second environment is not available because of python conflict with the system
[student03@ws2023-master ~]$ conda create -n Macs3 python=3.8.18
[student03@ws2023-master ~]$ conda install -c maximinio macs3

```

## Directory tree structure

```
.
|-- README.md
|-- lmartin_infos-generales_20231110
|-- log
|   |-- slurmjob-52300-0
|   ...
|-- results
|   |-- mapped_data
|   |   |-- ss50k_0h_R1_1_trim_paired_mapped.sam
|   |   |-- ss50k_0h_R2_1_trim_paired_mapped.sam
|   |   |-- ss50k_0h_R3_1_trim_paired_mapped.sam
|   |     ...
|   |-- mapped_data_bam_file
|   |   |-- ss50k_0h_R1_1_trim_paired_mapped.bam
|   |    ...
|   |   `-- ss50k_24h_R3_1_trim_paired_mapped.bam
|   |-- qc_init
|   |   |-- ss50k_0h_R1_1
|   |   |   `-- fastqc-init
|   |   |       |-- ss50k_0h_R1_1_fastqc.html
|   |   |       `-- ss50k_0h_R1_1_fastqc.zip
|   |   |-- ss50k_0h_R1_2
|   |   |   `-- fastqc-init
|   |   |       |-- ss50k_0h_R1_2_fastqc.html
|   |   |       `-- ss50k_0h_R1_2_fastqc.zip
|   |   |       ...
|   |-- qc_post
|   |   |-- ss50k_0h_R1_1_trim_paired
|   |   |   `-- qc_post
|   |   |       |-- ss50k_0h_R1_1_trim_paired_fastqc.html
|   |   |       `-- ss50k_0h_R1_1_trim_paired_fastqc.zip
|   |   |-- ss50k_0h_R1_1_trim_unpaired
|   |   |   `-- qc_post
|   |   |       |-- ss50k_0h_R1_1_trim_unpaired_fastqc.html
|   |   |       `-- ss50k_0h_R1_1_trim_unpaired_fastqc.zip
|   |   |         ...
|   `-- trimmed_data
|       |-- ss50k_0h_R1_1_trim_paired.fastq.gz
|       |-- ss50k_0h_R1_1_trim_unpaired.fastq.gz
|       |-- ss50k_0h_R1_2_trim_paired.fastq.gz
|       |-- ss50k_0h_R1_2_trim_unpaired.fastq.gz
|       |-- ss50k_0h_R2_1_trim_paired.fastq.gz
|       |-- ss50k_0h_R2_1_trim_unpaired.fastq.gz
|       |-- ss50k_0h_R2_2_trim_paired.fastq.gz
|       |-- ss50k_0h_R2_2_trim_unpaired.fastq.gz
|        ...
|       |-- stats
|       `-- trim.log
|-- scripts
|   |-- convert_sam-to-bam.slurm
|   |-- organisation-post-trim-file.sh
|   |-- stategra_align.slurm
|   |-- stategra_peak_calling.slurm
|   |-- stategra_qc-init.slurm
|   |-- stategra_qc-post.slurm
|   |-- stategra_remove-duplication.slurm
|   `-- stategra_trim.slurm
|-- slurm-52818.out
`-- stategra_wf.sh
```

## Authors and acknowledgment
Thank to my classmates and the professor, Mrs Nadia GOUE.

## License
Creative Commons Legal Code - CC0 1.0 Universal