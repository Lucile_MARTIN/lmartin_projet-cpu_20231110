#!/bin/bash
#cd "$(dirname "$0")"

echo 'Date: 11/12/2023'
echo 'Object: ATAC-seq sample workflow for Stategra datasets showing job execution and dependency handling.'
echo 'Inputs: paths to scripts for quality control, triming, mapping and peak calling'
echo 'Outputs: trimmed fastq files, QC HTML files, mapping SAM/BAM files, output of peak calling'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose

IFS=$'\n\t'


# first job - no dependencies
# Initial QC
jid1=$(sbatch --parsable scripts/stategra_qc-init.slurm)

echo "$jid1 : Initial Quality Control with Fastqc tool"

# Trimming
jid2=$(sbatch --parsable --dependency=afterok:$jid1 scripts/stategra_trim.slurm)

echo "$jid2 : Trimming with Trimmomatic tool"

# Organisation post-trim
jid3=$(sbatch --parsable --dependency=afterok:$jid2 scripts/organisation-post-trim-file.sh)

echo "$jid3 : Organisation post_trimming"

# Post QC
jid4=$(sbatch --parsable --dependency=afterok:$jid3 scripts/stategra_qc-post.slurm)

echo "$jid4 : Post control_quality with Fastqc tool"

# Mapping
jid5=$(sbatch --parsable --dependency=afterok:$jid4 scripts/stategra_align.slurm)

echo "$jid5 : Mapping with Bowtie2 tool "

# Convert SAM files to BAM files
jid6=$(sbatch --parsable --dependency=afterok:$jid5 scripts/convert_sam-to-bam.slurm)

echo "$jid6 : Conversion SAM to BAM with Samtools tool"

# Ending step considering job efficiency
#seff
